var form =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	__webpack_require__(1);
	__webpack_require__(3);
	module.exports = __webpack_require__(2);


/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	var formCounter = __webpack_require__(2);
	//var search = require('./search');

	var add = document.getElementById('add');
	var del = document.getElementById('del');
	var load = document.getElementById('load');
	var arr = new Array(0);

	////функция "загрузки" в localhost
	add.onclick = function () {
	    var dateTr = new Date();
	    var textarea = document.getElementById('textarea').value;
	    var title = document.getElementById('title').value;
	    var feedback = dateTr.getHours() + ":" + dateTr.getMinutes() + "<br>" + "Имя: " + title + "<br>Отзыв: " + textarea + "<br>";
	    if (arr.length < 5) {
	        arr.unshift(feedback);
	        localStorage.setItem("key", JSON.stringify(arr));
	    } else {
	        arr.pop();
	        arr.unshift(feedback);
	        localStorage.setItem("key", JSON.stringify(arr));
	    }
	    document.getElementById('feedback').reset();
	};

	del.onclick = function () {
	    localStorage.clear();
	};

	//функция "выгрузки" из localhost
	load.onclick = function () {
	    var itemList = document.getElementById("itemList");
	    var arr1 = [];
	    arr1 = JSON.parse(localStorage.getItem("key"));
	    itemList.innerHTML = "";
	    for (var i = 0; i < arr1.length; i++) {
	        var newItem = document.createElement("li");
	        newItem.innerHTML = arr1[i];
	        itemList.appendChild(newItem);
	    }
	};

/***/ },
/* 2 */
/***/ function(module, exports) {

	/*var title = document.getElementById('title');
	var textarea = document.getElementById('textarea');*/

	module.exports = function cField(max, title, warning, counter) {
	    title = document.getElementById(title);
	    var rest = max - title.value.length;
	    if (title.value.length > max) title.value = title.value.substr(0, max);
	    if (rest < 0) {
	        rest = 0;
	    }
	    document.getElementById(counter).firstChild.data = rest + "/" + max;
	    document.getElementById(warning).innerHTML = 'Ошибка! нельзя вводить больше ' + max + ' символа(-ов)';
	    if (rest > 0) document.getElementById(warning).innerHTML = '';
	};

	/*shest2.oninput = function () {
	    var max = 300;
	    var rest = max - this.value.length;
	    if (this.value.length > max) this.value = this.value.substr(0, max);
	    if (rest < 0) {rest = 0;}
	    document.getElementById("counter").firstChild.data = rest + "/" + max;
	    document.getElementById("warning").innerHTML = 'Ошибка! нельзя вводить больше ' + max + ' символа(-ов)';
	    if (rest > 0) document.getElementById("warning").innerHTML='';

	}*/

/***/ },
/* 3 */
/***/ function(module, exports) {

	var fil = document.getElementById('fil');
	var soure_text = '';
	var body_elem = document.getElementById('itemList');

	// функция поиска в тексте
	fil.onclick = function (text) {
	    soure_text = body_elem.innerHTML;
	    text = document.getElementById('input_text').value;
	    var body_re = soure_text.replace(text, '<p class="search">' + text + '</p>', 'g');
	    body_elem.innerHTML = body_re;
	    //document.getElementById('search').reset();
	};

	function replaceSource(e) {
	    if (soure_text && e.target.tagName.toLowerCase() != 'input') {
	        body_elem.innerHTML = soure_text;
	        soure_text = '';
	    }
	};
	// клик по документу
	document.addEventListener('click', replaceSource, false);

/***/ }
/******/ ]);