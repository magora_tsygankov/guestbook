
module.exports = {
    context: __dirname + '/frontend',
    entry: {
        form: ["./form.js", "./search.js", "./formCounter.js"]
    },
    output: {
        path: __dirname + "/public",
        filename: "build.js",
        library:  "[name]"
    },

    //watch: true,

    resolve: {
        modulesDirectories: ['node_modules']
    },
    module: {
        loaders: [
            {
                test: /\.js/,
                loader: 'babel-loader',
                exclue: /(node_modules|bower_components)/
            }
        ]
    }
};